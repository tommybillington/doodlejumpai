# -*- coding: utf-8 -*-
"""
	CopyLeft 2021 Michael Rouves

	This file is part of Pygame-DoodleJump.
	Pygame-DoodleJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Pygame-DoodleJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pygame-DoodleJump. If not, see <https://www.gnu.org/licenses/>.
"""

from random import randint
from pygame import Surface
import asyncio
from singleton import Singleton
from sprite import Sprite
import settings as config


# return True with a chance of: P(X=True)=1/x
chance = lambda x: not randint(0, x)


class Bonus(Sprite):
    """
    A class to represent a bonus
    Inherits the Sprite class.
    """

    WIDTH = 15
    HEIGHT = 15

    def __init__(self, parent: Sprite, color=config.GRAY,
                 force=config.PLAYER_BONUS_JUMPFORCE):

        self.parent = parent
        super().__init__(*self._get_initial_pos(),
                         Bonus.WIDTH, Bonus.HEIGHT, color)
        self.force = force

    def _get_initial_pos(self):
        x = self.parent.rect.centerx - Bonus.WIDTH // 2
        y = self.parent.rect.y - Bonus.HEIGHT
        return x, y


class Platform(Sprite):
    """
    A class to represent a platform.

    Should only be instantiated by a Level instance.
    Can have a bonus spring or broke on player jump.
    Inherits the Sprite class.
    """

    def __init__(self, x: int, y: int, width: int, height: int,
                 initial_bonus=False, breakable=False):

        color = config.PLATFORM_COLOR
        if breakable:
            color = config.PLATFORM_COLOR_LIGHT
        super().__init__(x, y, width, height, color)

        self.breakable = breakable
        self.__level = Level.instance
        self.__bonus = None
        if initial_bonus:
            self.add_bonus(Bonus)

    @property
    def bonus(self):
        return self.__bonus

    def add_bonus(self, bonus_type: type) -> None:
        assert issubclass(bonus_type, Bonus), "Not a valid bonus type !"
        if not self.__bonus and not self.breakable:
            self.__bonus = bonus_type(self)

    def remove_bonus(self) -> None:
        self.__bonus = None

    def onCollide(self) -> None:
        if self.breakable:
            self.__level.remove_platform(self)

    def draw(self, surface: Surface) -> None:
        super().draw(surface)
        if self.__bonus:
            self.__bonus.draw(surface)
        if self.camera_rect.y + self.rect.height > config.YWIN:
            self.__level.remove_platform(self)


class MovingPlatform(Platform):
    def __init__(self, x, y, width, height, speed, direction, *args, **kwargs):
        super().__init__(x, y, width, height, *args, **kwargs)
        self.speed = speed
        self.direction = direction

    def update(self):
        self.rect.x += self.speed * self.direction
        if self.rect.right >= config.XWIN or self.rect.left <= 0:
            self.direction *= -1
        
        # Update the position of the bonus (spring) if it exists
        if self.bonus:
            self.bonus.rect.x = self.rect.x + (self.rect.width - self.bonus.rect.width) // 2
            self.bonus.rect.y = self.rect.y - self.bonus.rect.height

    def draw(self, surface: Surface):
        super().draw(surface)

class Monster(Sprite):
    """
    A class to represent a platform.

    Should only be instantiated by a Level instance.
    Can have a bonus spring or broke on player jump.
    Inherits the Sprite class.
    """

    def __init__(self, x: int, y: int, width: int, height: int, moving=False):

        color = config.MOB_COLOR
        if moving:
            color = config.MOVING_MOB_COLOR
        super().__init__(x, y, width, height, color)

        self.__level = Level.instance
        

    # def onCollide(self) -> None: #Should be game over
    #     if self.breakable:
    #         self.__level.remove_platform(self)

    def draw(self, surface: Surface) -> None:
        super().draw(surface)
        if self.camera_rect.y + self.rect.height > config.YWIN:
            self.__level.remove_monster(self)

class MovingMonster(Monster):
    def __init__(self, x, y, width, height, speed, direction, moving=True):
        super().__init__(x, y, width, height, moving)
        self.speed = speed
        self.direction = direction

    def update(self):
        self.rect.x += self.speed * self.direction
        if self.rect.right >= config.XWIN or self.rect.left <= 0:
            self.direction *= -1
        
    def draw(self, surface: Surface):
        super().draw(surface)

    # def onCollide(self) -> None:
    #     #game over


class Level(Singleton):
    def __init__(self):
        self.platform_size = config.PLATFORM_SIZE
        self.max_platforms = config.MAX_PLATFORM_NUMBER
        self.distance_min = min(config.PLATFORM_DISTANCE_GAP)
        self.distance_max = max(config.PLATFORM_DISTANCE_GAP)

        self.monster_size = config.MONSTER_SIZE
        self.max_monsters = config.MAX_MONSTER_NUMBER
        self.monsterdistance_min = min(config.MONSTER_DISTANCE_GAP)
        self.monsterdistance_max = max(config.MONSTER_DISTANCE_GAP)

        self.bonus_platform_chance = config.BONUS_SPAWN_CHANCE
        self.breakable_platform_chance = config.BREAKABLE_PLATFORM_CHANCE

        self.__platforms = []
        self.__to_remove = []
        self.__monsters = []
        self.__removed_monsters = []

        self.__base_platform = Platform(
            config.HALF_XWIN - self.platform_size[0] // 2,  # X POS
            config.HALF_YWIN + config.YWIN / 3,  # Y POS
            *self.platform_size)  # SIZE

        self.__base_monster = Monster(
            config.HALF_XWIN - self.platform_size[0] // 2,  # X POS
            config.YWIN / 8,  # Y POS
            *self.monster_size)  # SIZE


    @property
    def platforms(self) -> list:
        return self.__platforms

    @property
    def monsters(self) -> list:
        return self.__monsters

    async def _generation(self) -> None:
        nb_to_generate = self.max_platforms - len(self.__platforms)
        for _ in range(nb_to_generate):
            self.create_platform()

    async def _generation_monsters(self) -> None:
        nb_to_generate = self.max_monsters - len(self.__monsters)
        for _ in range(nb_to_generate):
            self.create_monsters()


    def create_platform(self) -> None:
        if self.__platforms:
            offset = randint(self.distance_min, self.distance_max)
            if randint(0, 1): #randomly chooses between 0 and 1 for T/F
                new_platform = MovingPlatform(
                    randint(0, config.XWIN - self.platform_size[0]),
                    self.__platforms[-1].rect.y - offset,
                    *self.platform_size,
                    speed=randint(1, 3),
                    direction=randint(0, 1) * 2 - 1,
                    initial_bonus=chance(self.bonus_platform_chance),
                    breakable=chance(self.breakable_platform_chance))
            else:
                new_platform = Platform(
                    randint(0, config.XWIN - self.platform_size[0]),
                    self.__platforms[-1].rect.y - offset,
                    *self.platform_size,
                    initial_bonus=chance(self.bonus_platform_chance),
                    breakable=chance(self.breakable_platform_chance))
            self.__platforms.append(new_platform)
        else:
            self.__platforms.append(self.__base_platform)

    def create_monsters(self) -> None:
        if self.__monsters:
            offset = randint(self.monsterdistance_min, self.monsterdistance_max)    
            if randint(0, 1):
                new_monster = MovingMonster(
                    randint(0, config.XWIN - self.monster_size[0]),
                    self.__monsters[-1].rect.y - offset,
                    *self.monster_size,
                    speed=randint(1, 2),
                    direction=randint(0, 1) * 2 - 1)
            else:
                new_monster = Monster(
                    randint(0, config.XWIN - self.monster_size[0]),
                    self.__monsters[-1].rect.y - offset,
                    *self.monster_size)
            self.__monsters.append(new_monster)
        else:
            self.__monsters.append(self.__base_monster)

    def remove_monster(self, mob: Platform) -> bool: #change to the monster
        if mob in self.__monsters:
            self.__removed_monsters.append(mob)
            return True
        return False

    def remove_platform(self, plt: Platform) -> bool:
        if plt in self.__platforms:
            self.__to_remove.append(plt)
            return True
        return False

    def reset(self) -> None:
        self.__platforms = [self.__base_platform]
        self.__monsters = []

    def update(self) -> None:
        for platform in self.__to_remove:
            if platform in self.__platforms:
                self.__platforms.remove(platform)
        self.__to_remove = []

        for monster in self.__removed_monsters:
            if monster in self.__monsters:
                self.__monsters.remove(monster)
        self.__removed_monsters = []

        for platform in self.__platforms:
            if isinstance(platform, MovingPlatform):
                platform.update()

        for monster in self.__monsters:
            if isinstance(monster, MovingMonster):
                monster.update()

        asyncio.run(self._generation_monsters())
        asyncio.run(self._generation())

    def draw(self, surface: Surface) -> None:
        for platform in self.__platforms:
            platform.draw(surface)
        for monster in self.__monsters:
            monster.draw(surface)

